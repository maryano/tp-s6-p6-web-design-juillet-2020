<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('HomeController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(true);
$routes->set404Override();
$routes->setAutoRoute(true);//mila amboarina

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'HomeController::index');
$routes->get('/home', 'HomeController::index');
// $routes->add('product/(:any)','ProductController::index/$1');
// $routes->add('products/(:any)','ProductsController::index/$1');
// $routes->add('products','ProductsController::index');
$routes->add('fantech-admin/(:any)','ProductsController::admin/$1');
$routes->add('fantech-admin/','ProductsController::admin');

// REWRITING
$routes->add('login','LoginController::index');
$routes->add('logout','LoginController::logout');
$routes->add('login/log','LoginController::log');
$routes->add('product-update/(:any)','ProductUpdateController::index/$1');
$routes->add('product-add','ProductAddController::index');
$routes->add('updating-product','ProductUpdateController::update');
$routes->add('delete-product','ProductUpdateController::delete');
$routes->add('adding-product','ProductAddController::save');
// $routes->add('products-(:num).html','ProductsController::index/$1');
$routes->add('products.html','ProductsController::index');
// $routes->add('product-(:num).html','ProductController::index/$1');

$routes->add('product/([a-zA-Z0-9-_]*)-(:num).html','ProductController::index/$2');
$routes->add('category/(:num)-([a-zA-Z0-9-_]*).html','ProductsController::index/$1');



/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
