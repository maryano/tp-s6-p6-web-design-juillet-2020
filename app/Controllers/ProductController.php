<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\CategorieModel;
use App\Models\ProductModel;
use App\Models\UtilityModel;

class ProductController extends Controller
{
    public function index($id = null){
        //getting categories
        
        $category = new CategorieModel();
        $data['categories'] = $category->getExistingCategories();

        //product
        $pm = new ProductModel();
        if (is_null($id)){
            return redirect()->to(base_url());
        }else{
            $prd = $pm->getProductById($id);
            $data['prd']  = $prd;
            $data['m_category'] = $category->getCategorieName($prd[0]['id_categorie'])[0]['nom'];
            $uModel = new UtilityModel();
            $uModel->addCategoriesLink($data['categories']);
            //getting related products
            $data['related'] = $pm->where('id_categorie', $prd[0]['id_categorie'])->findAll();
        }
        $data['title'] = ucfirst('fantech Electronics : '.$prd[0]['nom']); // Capitalize the first letter
        echo view('templates/header', $data);
        echo view('pages/product', $data);
        echo view('templates/footer', $data);
    }   
   
}