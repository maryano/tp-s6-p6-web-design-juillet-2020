<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\CategorieModel;
use App\Models\ProductModel;
use App\Models\UtilityModel;

class ProductsController extends Controller
{
    public function index($main_category = null){
         //getting categories
         $category = new CategorieModel();
         $data['categories'] = $category->getExistingCategories();
         $pm = new ProductModel();
         if (is_null($main_category) || $main_category == ""){
             $data['m_category'] ='PRODUCTS FROM ALL CATEGORIES';
             $data['products'] = $pm->getProducts();
         }else{
             $data['m_category'] = $category->getCategorieName($main_category)[0]['nom'];
             $data['products'] = $pm->getProductByCategory($main_category);
         }
         //misy titre karazany roa eto x)
        $uModel = new UtilityModel();
        $uModel->addProductLink($data['products']);
        $uModel->addCategoriesLink($data['categories']);
         $data['title'] = ucfirst('fantech Electronics : '.$data['m_category'].' shopping, Madagascar shipping'); // Capitalize the first letter
         echo view('templates/header', $data);
         echo view('pages/products', $data);
         echo view('templates/footer', $data);
    }   
    public function admin($main_category = null){
        if(is_null(session('id'))){
            return redirect()->to(base_url('/login'));
        }
         //getting categories
         $category = new CategorieModel();
         $data['categories'] = $category->getExistingCategories();
         $uModel = new UtilityModel();
         $uModel->addCategoriesLink($data['categories']);
         $pm = new ProductModel();
         if (is_null($main_category)){
             $data['m_category'] ='PRODUCT FROM ALL CATEGORIES';
             $data['prd'] = $pm->getProducts();
         }else{
             $data['m_category'] = $category->getCategorieName($main_category)[0]['nom'];
             $data['prd'] = $pm->getProductByCategory($main_category);
         }
         //misy titre karazany roa eto x)
         $data['title'] = ucfirst('fantech admin'); // Capitalize the first letter
         $data['brand'] = ucfirst('admin'); // Capitalize the first letter
         echo view('templates/header', $data);
         echo view('pages/admin_home', $data);
         echo view('templates/footer', $data);
    }   
}