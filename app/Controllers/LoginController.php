<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\CategorieModel;
use App\Models\UserModel;

class LoginController extends BaseController
{
    public function __construct()
    {
        helper('url');
    }
    public function index($param = null, $val = null)
    {
        if( null !== session('id')){
            return redirect()->to(base_url('fantech-admin'));
        }
        $category = new CategorieModel();
        $data['categories'] = $category->getExistingCategories();

        $data['title'] = ucfirst('back-office'); // Capitalize the first letter
        $data['error'] = $param;
        $data['value'] = $val;
        echo view('templates/header', $data);
        echo view('pages/login', $data);
        echo view('templates/footer', $data);
    }
    public function log()
    {
        $user = new UserModel();
        helper('form');
        $value = array(
            "pseudo" => $this->request->getPost('pseudo'),
            "password" => $this->request->getPost('password')
        );
        #verify if post method, and validatings
        if ($this->request->getMethod() === 'post' && $this->validate($user->getValidationRules())) {
            #check login informations
            $user_object = $user->login($this->request->getPost('pseudo'), $this->request->getPost('password'));
            if (!is_null($user_object)) {
                #data for session
                $newdata = [
                    'id'  => $user_object['id'],
                    'pseudo'     => $user_object['pseudo'],
                    'logged_in' => TRUE
                ];
                session()->set($newdata);
                #redirecting
                return redirect()->to(base_url('fantech-admin'));
                // redirect(base_url('/fantech-admin'));
                // echo view('pages/about');
            } else {
                $error = "Connexion error, verify your informations";
                $this->index($error, $value);
            }
        } else {
            $error = "validation error";
            $this->index($error, $value);
        }
    }
    public function logout(){
        // unset(
        //     session('id'),
        //     session('pseudo')
        // );
        $this->session->remove('id');
        $this->session->remove('pseudo');
        session()->set('logged_in',FALSE);
        return redirect()->to(base_url('/'));
    }
    public function deprecated_log()
    {
        //getting categories
        $user = new UserModel();
        if ($this->request->getMethod() === 'post') {
            if ($this->validate($user->getValidationRules())) {
                if ($user->login($this->request->getPost('pseudo'), $this->request->getPost('pseudo'))) {
                    // echo view('templates/header', $data);
                    echo view('pages/about');
                    // echo view('templates/footer', $data);
                } else {
                    $category = new CategorieModel();
                    $data['categories'] = $category->getExistingCategories();

                    $data['title'] = ucfirst('back-office'); // Capitalize the first letter
                    $data['error'] = 'connexion error, verify your informations';
                    echo view('templates/header', $data);
                    echo view('pages/login', $data);
                    echo view('templates/footer', $data);
                }
            } else {
                $category = new CategorieModel();
                $data['categories'] = $category->getExistingCategories();

                $data['title'] = ucfirst('error'); // Capitalize the first letter
                $data['errors'] = $user->getValidationMessages();
                echo view('templates/header', $data);
                echo view('pages/login', $data);
                echo view('templates/footer', $data);
            }
        } else if ($this->request->getMethod() === 'get') {
            $category = new CategorieModel();
            $data['categories'] = $category->getExistingCategories();

            $data['title'] = ucfirst('back-office'); // Capitalize the first letter

            echo view('templates/header', $data);
            echo view('pages/login', $data);
            echo view('templates/footer', $data);
        }
    }
}
