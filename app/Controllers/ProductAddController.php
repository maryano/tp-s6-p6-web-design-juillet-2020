<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\CategorieModel;
use App\Models\ProductModel;
use App\Models\UtilityModel;

class ProductAddController extends Controller
{
    public function __construct()
    {
        helper('url', 'form');
    }
    public function index($param = null)
    {
        if (is_null(session('id'))) {
            return redirect()->to(base_url('/login'));
        }
        //getting categories
        $category = new CategorieModel();
        $data['all_categories'] = $category->getCategories();
        $data['categories'] = $category->getExistingCategories();
        $data['brand'] = ucfirst('fantech-admin'); // Capitalize the first letter
        //product
        $pm = new ProductModel();
        $data['msg'] = $param;
        $data['title'] = ucfirst('admin'); // Capitalize the first letter
        $data['brand'] = ucfirst('admin'); // Capitalize the first letter

        //
        $uModel = new UtilityModel();
        $uModel->addCategoriesLink($data['categories']);

        echo view('templates/header', $data);
        echo view('pages/product_add', $data);
        echo view('templates/footer', $data);
    }
    public function save()
    {
        if ($this->request->getMethod() === 'post') {
            //upload
            $file = $this->request->getFile('avatar');
            $file_name = $file->getName();
            
            $validated = $this->validate([
                'avatar' => [
                    'mime_in[avatar,image/jpg,image/jpeg,image/gif,image/png]',
                    'max_size[avatar,4096]',
                ]
            ]);
            $u = new UtilityModel();
            $file_name = $u->formatImg($file_name);
            if ($file->isValid() && $validated && !$file->hasMoved()) {
                $file->move('pictures', $file_name);
            } else {
                $msg = array('an error occurs during image upload, please choose .jpg, .gif or .png images');
                $this->index($msg);
            }
            $file_path = 'public/pictures/' . $file_name;
            $product_name = $this->request->getPost('product_name');
            $product_price = $this->request->getPost('product_price');
            $product_category = $this->request->getPost('product_category'); //upload
            $product_description = $this->request->getPost('product_description');
            $product_image_alt = $this->request->getPost('product_image_alt');

            $data = [
                'nom' => $product_name,
                'prix'    => $product_price,
                'id_categorie'    => $product_category,
                'description'    => $product_description,
                'img'    => $file_path,
                'alt'    => $product_image_alt
            ];
            $product = new ProductModel();
            if ($product->save($data) === false) {
                $this->index($product->errors());
            }else{
                 $c =new ProductsController();
                 $c->admin($product_category);
            }
        } else {
            //redirigena
            return redirect()->to(base_url('fantech-admin'));
        }
    }
}
