<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\CategorieModel;
use App\Models\ProductModel;
use App\Models\USerModel;

class Pages extends Controller
{
    public function index()
    {
        return view('welcome_message');
    }

    // public function view($page = 'home')
    // {
    //     if ( ! is_file(APPPATH.'/Views/pages/'.$page.'.php'))
    //     {
    //         // 404
    //         throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
    //     }
    
    //     $data['title'] = ucfirst($page); // Capitalize the first letter
        
    //     echo view('templates/'.$page.'_head', $data);
    //     echo view('templates/header', $data);
    //     echo view('pages/'.$page, $data);
    //     echo view('templates/footer', $data);
    // }
    public function home(){
        //getting categories
       
            $category = new CategorieModel();
            $data['categories'] = $category->getExistingCategories();
    
            $data['title'] = ucfirst('fantech : online shopping for Electronics, madagascar, smartphone, tablet, computer'); // Capitalize the first letter
            echo view('templates/header', $data);
            echo view('pages/home', $data);
            echo view('templates/footer', $data);
        
    }
    public function products($main_category = null){
        //getting categories
        $category = new CategorieModel();
        $data['categories'] = $category->getExistingCategories();
        $pm = new ProductModel();
        if (is_null($main_category)){
            $data['m_category'] ='PRODUCT FROM ALL CATEGORIES';
            $data['prd'] = $pm->getProducts();
        }else{
            $data['m_category'] = $category->getCategorieName($main_category)[0]['nom'];
            $data['prd'] = $pm->getProductByCategory($main_category);
        }
        //misy titre karazany roa eto x)
        $data['title'] = ucfirst('fantech.com : smartphones'); // Capitalize the first letter
        echo view('templates/header', $data);
        echo view('pages/products', $data);
        echo view('templates/footer', $data);
    }
    public function product($id = null){
        //getting categories
        $category = new CategorieModel();
        $data['categories'] = $category->getExistingCategories();

        //product
        $pm = new ProductModel();
        if (is_null($id)){
            //
        }else{
            $prd = $pm->getProductById($id);
            $data['prd']  = $prd;
            $data['m_category'] = $category->getCategorieName($prd[0]['id_categorie'])[0]['nom'];
        }
        $data['title'] = ucfirst('fantech.com : Iphone X - 32 GB - gris sideral'); // Capitalize the first letter
        echo view('templates/header', $data);
        echo view('pages/product', $data);
        echo view('templates/footer', $data);
    }
    public function login(){
        //getting categories
        $user = new UserModel();
        if($this->request->getMethod() === 'post'){
            if($this -> validate($user->getValidationRules())){
                if($user -> login($this->request->getPost('pseudo'),$this->request->getPost('pseudo'))){
                    // echo view('templates/header', $data);
                    echo view('pages/about');
                    // echo view('templates/footer', $data);
                }else{
                    $category = new CategorieModel();
                    $data['categories'] = $category->getExistingCategories();
                    
                    $data['title'] = ucfirst('back-office'); // Capitalize the first letter
                    $data['error'] = 'connexion error, verify your informations';
                    echo view('templates/header', $data);
                    echo view('pages/login', $data);
                    echo view('templates/footer', $data);
                }
            }else{
                $category = new CategorieModel();
                    $data['categories'] = $category->getExistingCategories();
                    
                    $data['title'] = ucfirst('error'); // Capitalize the first letter
                    $data['errors'] = $user -> getValidationMessages() ;
                    echo view('templates/header', $data);
                    echo view('pages/login', $data);
                    echo view('templates/footer', $data);
            }
        }else if($this->request->getMethod() === 'get'){
            $category = new CategorieModel();
            $data['categories'] = $category->getExistingCategories();
            
            $data['title'] = ucfirst('back-office'); // Capitalize the first letter
            
            echo view('templates/header', $data);
            echo view('pages/login', $data);
            echo view('templates/footer', $data);
        }
    }
    
    public function test(){
        $pm = new ProductModel();
        $data['prd'] = $pm->getProductByCategory(3);
        $data['title'] = ucfirst('fantech.com : Iphone X - 32 GB - gris sideral'); // Capitalize the first letter
        echo view('pages/about', $data);
    }
}