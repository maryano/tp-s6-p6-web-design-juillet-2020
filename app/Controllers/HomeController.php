<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\CategorieModel;
use App\Models\ProductModel;
use App\Models\UtilityModel;

class HomeController extends Controller
{
    public function index()
    {
        //getting categories
        $category = new CategorieModel();
        $data['categories'] = $category->getExistingCategories();

        //getting all products
        $productModel = new ProductModel();
        
        $data['products'] = $productModel->findAll();
        $uModel = new UtilityModel();
        $uModel->addProductLink($data['products']);
        $uModel->addCategoriesLink($data['categories']);
       
        
        $data['title'] = ucfirst('fantech : online shopping for Electronics ( smartphones, laptops, cameras, accessories and more), Madagascar shipping'); // Capitalize the first letter
        echo view('templates/header', $data);
        echo view('pages/home', $data);
        echo view('templates/footer', $data);
    }
}
