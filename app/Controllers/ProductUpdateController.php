<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\CategorieModel;
use App\Models\ProductModel;
use App\Models\UtilityModel;

class ProductUpdateController extends Controller
{
    public function index($id = null){
        //getting categories
        $category = new CategorieModel();
        $data['categories'] = $category->getExistingCategories();
        
        $data['brand'] = ucfirst('fantech-admin'); // Capitalize the first letter
        //product
        $pm = new ProductModel();
        if (is_null($id) || $id == ""){
            return redirect()->to(base_url('fantech-admin'));
        }else{
            $prd = $pm->getProductById($id);
            $data['prd']  = $prd;
            $data['m_category'] = $category->getCategorieName($prd[0]['id_categorie'])[0]['nom'];
        }
        if(count($data['prd']) > 0) {
            $data['title'] = ucfirst('fantech-admin'); // Capitalize the first letter
            $data['brand'] = ucfirst('admin'); // Capitalize the first letter
            echo view('templates/header', $data);
            echo view('pages/product_update', $data);
            echo view('templates/footer', $data);
        }else{
            return redirect()->to(base_url('fantech-admin'));
        }
    }   
    public function update(){
        if ($this->request->getMethod() === 'post'){
            $product_id = $this->request->getPost('product_id');
            $product_name = $this->request->getPost('product_name');
            $product_price = $this->request->getPost('product_price');
            $product_description = $this->request->getPost('product_description');
            $product_image_alt = $this->request->getPost('product_image_alt');
            $data = [
                'nom' => $product_name,
                'prix'    => $product_price,
                'description'    => $product_description,
                'alt'    => $product_image_alt
            ];
            $product = new ProductModel();
            // echo view('pages/about', $data);
            try{
                $product->update($product_id, $data);
                $this->index($product_id);
            }
            catch (\Exception $e)
            {
                die($e->getMessage());
            }
        }else{
            //redirigena
            return redirect()->to(base_url('fantech-admin'));
        }
    }
    public function delete(){
        if ($this->request->getMethod() === 'post'){
            $product_id = $this->request->getPost('product_id');
            $product = new ProductModel();
            try{
                if(!is_null($product_id) && $product != ""){
                $product->delete($product_id);
                }
                return redirect()->to(base_url('fantech-admin'));
            }
            catch (\Exception $e)
            {
                $data['e'] = $e->getMessage();
                echo view('pages/about', $data);
            }
        }else{
            //redirigena
            return redirect()->to(base_url('fantech-admin'));
        }
    }
    
    
}