<div style="height : 200px;" class="footer_overlay"></div>
<footer class="footer">
    <div class="footer_background" style="background-image:url(<?php echo base_url('public/images/footer.jpg'); ?>)"></div>
    <div class="container">
        
        <div class="row">
            <div class="col">
                <div class="w-100 p-4 d-flex flex-lg-row flex-column align-items-center justify-content-lg-start justify-content-center">
                    <div class="footer_logo"><a href="#">FanTech electronics.</a></div>
                    <div class="copyright ml-auto mr-auto">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | Mariano</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                    <div class="footer_social ml-lg-auto">
                        <ul>
                            <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pl-4 pr-4 pt-3">
            <div class="col-lg-6 pb-3">
                <h4 style="color:black;">Another categories : </h4>
                <p>
                    <?php
                    $list = '';
                    for ($i = 0; $i < count($categories); $i++) {
                        $list .= '<a style="font-weight : 500; color:white;border : 0;text-shadow: 1px 1px 6px black;" href="'.base_url($categories[$i]->link).'">' . $categories[$i]->nom . '</a> | ';
                    }
                    echo $list;
                    ?>
                </p>
            </div>
            <div class="col-lg-6">
                <h4 style="color:black;">Another ressources : </h4>
                <p>
                   <a href="https://www.amazon.com" style="font-weight : 500;color:white;border : 0;text-shadow: #000 1px 1px 6px;">Amazon</a> | 
                   <a href="https://www.aliexpress.com" style="font-weight : 500;color:white;border : 0;text-shadow: #000 1px 0 10px;">aliexpress</a> | 
                   <a href="https://eu.mouser.com" style="font-weight : 500;color:white;border : 0;text-shadow: #000 1px 0 10px;">mouser Electronics</a> | 
                   <a href="https://www.ketrika.com" style="font-weight : 500;color:white;border : 0;text-shadow: #000 1px 0 10px;">ketrika.com</a> | 
            </div>
        </div>
    </div>
</footer>
</div>
<script src="<?php echo base_url('public/js/jquery-3.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('public/styles/bootstrap4/popper.js') ?>"></script>
<script src="<?php echo base_url('public/styles/bootstrap4/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('public/plugins/greensock/TweenMax.min.js') ?>"></script>
<script src="<?php echo base_url('public/plugins/greensock/TimelineMax.min.js') ?>"></script>
<script src="<?php echo base_url('public/plugins/scrollmagic/ScrollMagic.min.js') ?>"></script>
<script src="<?php echo base_url('public/plugins/greensock/animation.gsap.min.js') ?>"></script>
<script src="<?php echo base_url('public/plugins/greensock/ScrollToPlugin.min.js') ?>"></script>
<script src="<?php echo base_url('public/plugins/OwlCarousel2-2.2.1/owl.carousel.js') ?>"></script>
<script src="<?php echo base_url('public/plugins/Isotope/isotope.pkgd.min.js') ?>"></script>
<script src="<?php echo base_url('public/plugins/easing/easing.js') ?>"></script>
<script src="<?php echo base_url('public/plugins/parallax-js-master/parallax.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/custom.js') ?>"></script>
<script src="<?php echo base_url('public/js/categories.js') ?>"></script>
<script src="<?php echo base_url('public/js/product.js') ?>"></script>




</body>

</html>