<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/checkout.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/checkout_responsive.css') ?>">
<div class="home">
		<div class="home_container">
			<div class="home_background" style="background-image:url(<?php echo base_url('public/images/cart.jpg');?>)"></div>
			<div class="home_content_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="home_content">
								<div class="breadcrumbs">
									<ul>
										<li><a href="index.html">PLEASE LOG IN.</a></li>
										<!-- <li><a href="categories.html"><?php echo $m_category ?></a></li>
										<li><?php echo $prd[0]['nom'] ?></li> -->
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <div class="checkout">
		<div class="container">
			<div class="row">

				<!-- Billing Info -->
				<div class="col-lg-10">
					<div class="billing checkout_section">
						<div class="section_title">Enter your user information</div>
						<div class="section_subtitle">terms & conditions.</div>
						<div class="checkout_form_container">
							<form action="<?php echo base_url('login/log'); ?>" id="checkout_form" class="checkout_form" method = 'post'>
								<div class="row">
									<div class="col-xl-6">
										<!-- Name -->
										<label for="checkout_name">pseudo</label>
										<?php if(!is_null($value)) {?>
										<input name="pseudo" type="text" class="checkout_input" required="required" value="<?php echo $value['pseudo'] ?>">
										<?php } else{?>
										<input name="pseudo" type="text" class="checkout_input" required="required" >
										<?php } ?>
									</div>
									<div class="col-xl-6 last_name_col">
										<!-- Last Name -->
										<label for="checkout_last_name">password</label>
										<?php if(!is_null($value)) {?>
										<input name="password" type="password" class="checkout_input" required="required" value="<?php echo $value['password'] ?>">
										<?php } else{?>
										<input name="password" type="password" class="checkout_input" required="required" >
										<?php } ?>
                                    </div>
                                </div>
                                <?php if (! empty($error)) { ?>
                                   
                                        <p style = "margin-top:10px;color: red;"><?= $error ?></p>
                                    
                                <?php } ?>
                                <input class="button order_button" type="submit" value ="connect" />
                                
							</form>
						</div>
                    </div>
				</div>
                
				<!-- Order Info -->

				
			</div>
		</div>
	</div>