<!-- Home -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/checkout_responsive.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/checkout.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/product.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/product_responsive.css') ?>">
<div class="home">
    <div class="home_container">
        <div class="home_background" style="background-image:url(<?php echo base_url('public/images/cart.jpg'); ?>)"></div>
        <div class="home_content_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content">
                            <div class="breadcrumbs">
                                <ul>
                                    <li><a href="<?= base_url('fantech-admin') ?>">admin Home</a></li>
                                    <li><?php echo substr($prd[0]['nom'], 0, 100); ?>...</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Product Details -->
<style>
    .mariano_floater{
        z-index : 2000;
        position: fixed;
        right : 10px;
        bottom : 20px;
    }
    .mariano_custom_button{
        padding : 10px;
        border-radius: 5px;
        background-color: red;
        color : white;
        border : none;
    }
    .mariano_custom_button:hover{
        border : 1px solid red;
        color : red;
        background-color: white;
    }
</style>
<div class="product_details">
    <div class="container">
        <form method="post" action="<?= base_url('delete-product') ?>">
            <div class="mariano_floater">
                <input type="hidden" value="<?= $prd[0]['id'] ?>" name="product_id" >
                <input type="submit" class="mariano_custom_button" value="Delete this product" >
            </div>
        </form>
        <form action="<?php echo base_url('updating-product'); ?>" method="post" id="checkout_form" class="checkout_form">
            <div class="row details_row">
                <!-- Product Image -->
                <div class="col-lg-6">
                    <div class="details_image">
                        <div class="details_image_large"><img src="<?php echo base_url($prd[0]['img']); ?>" alt="<?php echo $prd[0]['alt'] ?>">
                            <div class="product_extra product_new"><a href="categories.html">promo</a></div>
                        </div>
                        <label style="margin-top : 10px; " for="product_image_alt">product image alt</label>
                        <textarea name="product_image_alt" style="font-size : 17px; height : 125px; padding-top : 10px;padding-bottom : 20px; " type="text" class="checkout_input" required="required"><?php echo $prd[0]['alt']; ?></textarea>
                        <!-- <div class="details_image_thumbnails d-flex flex-row align-items-start justify-content-between"> -->
                        <!-- <div class="details_image_thumbnail active" data-image="public/images/details_1.jpg"><img src="public/images/details_1.jpg" alt=""></div> -->
                        <!-- <div class="details_image_thumbnail" data-image="public/images/details_3.jpg"><img src="public/images/details_3.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_2.jpg"><img src="public/images/details_2.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div> -->
                        <!-- </div> -->
                    </div>
                </div>

                <!-- Product Content -->
                <div class="col-lg-6">
                    <div class="details_content">

                        <label for="product_name">Product name</label>
                        <textarea name="product_name" style="font-size : 17px; height : 125px; padding-top : 20px;padding-bottom : 20px; " type="text" class="checkout_input" required="required"><?php echo $prd[0]['nom']; ?></textarea>

                        <label style="margin-top : 10px;" for="product_price">product price in $</label>
                        <input type="text" name="product_price" class="checkout_input" value="<?php echo $prd[0]['prix'] ?>">
                        <input type="hidden" name="product_id" value="<?php echo $prd[0]['id'] ?>">


                        <!-- In Stock -->
                        <div class="in_stock_container">
                            <div class="availability">Availability:</div>
                            <span>In Stock</span>
                        </div>
                        <div class="details_text">
                            <label for="product_description">product description</label>
                            <textarea name="product_description" style="font-size : 15px; height : 250px; padding-top : 20px;padding-bottom : 20px; " type="text" class="checkout_input"><?php echo $prd[0]['description']; ?></textarea>
                            <!-- <p><?php echo $prd[0]['description'] ?></p> -->
                        </div>

                        <!-- Product Quantity -->
                        <div class="product_quantity_container">
                            <input class="button order_button" type="submit" value="save modifications" />
                        </div>

                        <!-- Share -->
                        <div class="details_share">
                            <span>Share:</span>
                            <ul>
                                <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
      
            
      
        <div class="row description_row pb-4">
            <div class="col">
                <div class="description_title_container">
                    <div class="description_title">Description</div>
                    <div class="reviews_title"><a href="#">Reviews <span>(1)</span></a></div>
                </div>
                <div class="description_text">
                    <p><?php echo $prd[0]['description'] ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Products -->



<!-- Newsletter -->

<div class="newsletter" style = "background : none;">
    <div class="container">

    </div>
</div>