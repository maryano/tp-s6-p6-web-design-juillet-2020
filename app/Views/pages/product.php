<!-- Home -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/product.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/product_responsive.css') ?>">
<div class="home">
    <div class="home_container">
        <div class="home_background" style="background-image:url(<?php echo base_url('public/images/cart.jpg'); ?>)"></div>
        <div class="home_content_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content">
                            <h4 class="breadcrumbs">
                                <ul>
                                    <li><a href="<?= base_url() ?>">Home</a></li>
                                    <li><a href="<?= base_url('products-'.$prd[0]['id_categorie'].".html") ?>"><?php echo $m_category ?></a></li>
                                    <!-- <li><?php echo substr($prd[0]['nom'], 0, 100); ?>...</li> -->
                                    <li><?php
										// echo substr($products[$i]['nom'],0,50); 
										if (strlen($prd[0]['nom']) > 100) {
											echo substr($prd[0]['nom'], 0, 100) . '...';
											// echo (strlen($products[$i]['nom']));
										} else {
											echo ($prd[0]['nom']);
										}
										?></li>
                                </ul>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Product Details -->

<div class="product_details">
    <div class="container">
        <div class="row details_row">

            <!-- Product Image -->
            <div class="col-lg-6">
                <div class="details_image">
                    <div class="details_image_large"><img src="<?php echo base_url($prd[0]['img']); ?>" alt="<?php echo $prd[0]['alt'] ?>">
                        <div class="product_extra product_new"><a href="categories.html">promo</a></div>
                    </div>
                    <!-- <div class="details_image_thumbnails d-flex flex-row align-items-start justify-content-between"> -->
                    <!-- <div class="details_image_thumbnail active" data-image="public/images/details_1.jpg"><img src="public/images/details_1.jpg" alt=""></div> -->
                    <!-- <div class="details_image_thumbnail" data-image="public/images/details_3.jpg"><img src="public/images/details_3.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_2.jpg"><img src="public/images/details_2.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div>
                        <div class="details_image_thumbnail" data-image="public/images/details_4.jpg"><img src="public/images/details_4.jpg" alt=""></div> -->
                    <!-- </div> -->
                </div>
            </div>

            <!-- Product Content -->
            <div class="col-lg-6">
                <div class="details_content">
                    <h1 class="details_name"><?php echo $prd[0]['nom'] ?></h1>
                    <h2 class="details_price">$ <?php echo $prd[0]['prix'] ?></h2>

                    <!-- In Stock -->
                    <div class="in_stock_container">
                        <div class="availability">Availability:</div>
                        <span>In Stock</span>
                    </div>
                    <div class="details_text">
                        <h2><?php echo $prd[0]['description'] ?></h2>
                    </div>

                    <!-- Product Quantity -->
                    <div class="product_quantity_container">
                        <div class="product_quantity clearfix">
                            <span>Qty</span>
                            <input id="quantity_input" type="text" pattern="[0-9]*" value="1">
                            <div class="quantity_buttons">
                                <div id="quantity_inc_button" class="quantity_inc quantity_control"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
                                <div id="quantity_dec_button" class="quantity_dec quantity_control"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <div class="button cart_button"><a href="#">Add to cart</a></div>
                    </div>

                    <!-- Share -->
                    <div class="details_share">
                        <span>Share:</span>
                        <ul>
                            <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row description_row">
            <div class="col">
                <div class="description_title_container">
                    <div class="description_title">Description</div>
                    <div class="reviews_title"><a href="#">Reviews <span>(1)</span></a></div>
                </div>
                <div class="description_text">
                    <p><?php echo $prd[0]['description'] ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Products -->

<div class="products">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <div class="products_title">Related Products</div>
            </div>
        </div>
        <div class="row">
            <div class="col">

                <div class="product_grid">
                    <!-- Product -->
                        <?php for($i = 0 ; $i < count($related) ;$i++){?>
                            <?php if($related[$i]['id'] != $prd[0]['id']){ ?>
						<div class="product">
						<div class="product_image"><img style=" height : 200px; " src="<?php echo base_url($related[$i]['img']); ?>" alt="<?php echo $related[$i]['alt'] ?>"></div>
							<!-- <div class="product_extra product_new"><a href="categories.html">New</a></div> -->
							<div class="product_content">
								<div class="product_title"><a href="<?php echo base_url('product/'.$related[$i]['id']); ?>"><?php echo substr($related[$i]['nom'],0,50); ?>...</a></div>
								<div class="product_price">$ <?php echo $related[$i]['prix'] ?></div>
							</div>
                        </div>
                            <?php } ?>
						<?php } ?>
                   

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Newsletter -->

<div class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="newsletter_border"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="newsletter_content text-center">
                    <div class="newsletter_title">Subscribe to our newsletter</div>
                    <div class="newsletter_text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed nec molestie eros</p>
                    </div>
                    <div class="newsletter_form_container">
                        <form action="#" id="newsletter_form" class="newsletter_form">
                            <input type="email" class="newsletter_input" required="required">
                            <button class="newsletter_button trans_200"><span>Subscribe</span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>