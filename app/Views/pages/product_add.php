<!-- Home -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/checkout_responsive.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/checkout.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/product.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/styles/product_responsive.css') ?>">
<div class="home">
    <div class="home_container">
        <div class="home_background" style="background-image:url(<?php echo base_url('public/images/cart.jpg'); ?>)"></div>
        <div class="home_content_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="home_content">
                            <div class="breadcrumbs">
                                <ul>
                                    <li><a href="<?= base_url('fantech-admin') ?>">admin Home</a></li>
                                    <li>add new product</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Product Details -->

<div class="product_details">
    <div class="container">
        <form action="<?php echo base_url('adding-product'); ?>" method="post" id="checkout_form" class="checkout_form" enctype="multipart/form-data">
            <div class="row details_row">
                <!-- Product Image -->
                <div class="col-lg-6">
                    <div>
                        <!-- Country -->
                        <label for="checkout_country">product category</label>
                        <select name="product_category" id="checkout_country" class="dropdown_item_select checkout_input">
                            <?php for ($i = 0; $i < count($all_categories); $i++) { ?>
                                <option value="<?php echo $all_categories[$i]['id']; ?>"><?php echo $all_categories[$i]['nom']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="details_image">
                        <label style="margin-top : 10px;" for="product_image_alt">product image</label>
                        <input required="required" name="avatar" style="font-size : 17px; padding-top : 10px;padding-bottom : 20px; " type="file" class="checkout_input"><?php echo $prd[0]['alt']; ?>
                        <label style="margin-top : 10px; " for="product_image_alt">product image alt</label>
                        <textarea required="required" name="product_image_alt" style="font-size : 17px; height : 125px; padding-top : 10px;padding-bottom : 20px; " type="text" class="checkout_input"></textarea>
                        <!-- <div class="product_quantity_container"> -->
                        
                            <?php if (!empty($msg)) : ?>
                                <div class="alert alert-danger">
                                    <?php foreach ($msg as $field => $m) : ?>
                                        <p><?= $m ?></p>
                                    <?php endforeach ?>
                                </div>
                            <?php endif ?>
                        
                        <input required="required" style="margin-top:10px;" class="button" type="submit" value="Add this" />
                        <!-- <div class="button cart_button"><a href="#">save modifications</a></div> -->
                        <!-- </div> -->
                    </div>
                </div>

                <!-- Product Content -->
                <div class="col-lg-6">
                    <div class="details_content">

                        <label for="product_name">Product name</label>
                        <textarea required="required" placeholder="ex : iphone x 32GB grey" name="product_name" style="font-size : 17px; height : 125px; padding-top : 10px;padding-bottom : 20px; " type="text" class="checkout_input"></textarea>

                        <label style="margin-top : 10px;" for="product_price">product price in $</label>
                        <input required="required" type="text" name="product_price" class="checkout_input">

                        <div class="details_text">
                            <label for="product_description">product description</label>
                            <textarea required="required" name="product_description" style="font-size : 15px; height : 250px; padding-top : 20px;padding-bottom : 10px; " type="text" class="checkout_input"></textarea>

                        </div>

                        <!-- Product Quantity -->


                        <!-- Share -->
                        <div class="details_share">
                            <span>Share:</span>
                            <ul>
                                <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row description_row">
            <div class="col">
                <div class="description_title_container">
                    <div class="description_title">Description</div>
                    <div class="reviews_title"><a href="#">Reviews <span>(1)</span></a></div>
                </div>
                <div class="description_text">
                    <p><?php echo $prd[0]['description'] ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Products -->



<!-- Newsletter -->

<div class="newsletter">
    <div class="container">

    </div>
</div>