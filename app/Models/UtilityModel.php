<?php

namespace App\Models;

use CodeIgniter\Model;

class UtilityModel extends Model
{
    public function formatUrl($str, $sep = '-')
    {
        $res = strtolower($str);
        $res = preg_replace('/[^[:alnum:]]/', ' ', $res);
        $res = preg_replace('/[[:space:]]+/', $sep, $res);
        return trim($res, $sep);
    }
    public function formatImg($str, $sep = '-')
    {
        $res = strtolower($str);
        $res = preg_replace('/[^[:alnum:]|.]/', ' ', $res);
        $res = preg_replace('/[[:space:]]+/', $sep, $res);
        return trim($res, $sep);
    }
    function addProductLink(&$list){
        for($i = 0 ; $i < count($list);$i++){
        $formated_name = $this->formatUrl($list[$i]['nom']);
        $list[$i]['link'] = 'product/'.$formated_name.'-'.$list[$i]['id'].'.html';
        }
        return $list;
    }
    function addCategoriesLink(&$list){
        for($i = 0 ; $i < count($list);$i++){
        $formated_name = $this->formatUrl($list[$i]->nom);
        $list[$i]->link = 'category/'.$list[$i]->id.'-'.$formated_name.'.html';
        }
        return $list;
    }
}
