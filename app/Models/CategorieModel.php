<?php namespace App\Models;

use CodeIgniter\Model;

class CategorieModel extends Model{
    public function __construct()
    {
        parent::__construct();
    }
    protected $table = 'categorie';

    public function getCategories(){
        return $this->findAll();
    } 
    public function getCategorieName($id){
        return $this->where('id',$id)->findAll();
    } 
    public function getExistingCategories(){
        $builder = $this -> db ->table('real_categories');
        return $builder->get()->getResult();  // Produces: SELECT * FROM categorie
    }
}
