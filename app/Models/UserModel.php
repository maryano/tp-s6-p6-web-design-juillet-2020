<?php 
namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model{
    protected $table = 'users';
    protected $validationRules = [
        'pseudo' => 'required|min_length[4]',
        'password' => 'required|min_length[4]'
    ];
    protected $validationMessages = [
        'pseudo' => 'sorry, your username should be 4 charachters or more'
    ];

    public function __construct()
    {
        parent::__construct();
    }
    public function getUsers(){
        return $this->findAll();
    }
    
    public function login($pseudo,$password){
        $user = $this->where('pseudo', $pseudo)->findAll();
        if(strcmp($user[0]['password'],$password) === 0 ){
            return $user[0];
        }else{
            return null;
        }
    }
    
}