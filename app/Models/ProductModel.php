<?php 
namespace App\Models;

use CodeIgniter\Model;

class ProductModel extends Model{
    protected $table = 'produit';
    protected $allowedFields = ['nom', 'prix','description','alt','img','id_categorie'];
    protected $deletedField  = 'deleted_at';
    protected $validationRules    = [
        'nom'     => 'required',
        'prix'        => 'required',
        'description'     => 'required',
        'alt' => 'required',
        'id_categorie' => 'required',
    ];
    public function __construct()
    {
        parent::__construct();
    }
    public function getProducts(){
        return $this->findAll();
    }
    public function getProductByCategory($id_category){
        return $this->where('id_categorie', $id_category)->findAll();
    }
    public function getProductById($id){
        return $this->where('id', $id)->findAll();
    }
}