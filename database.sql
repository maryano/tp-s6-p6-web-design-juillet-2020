CREATE DATABASE fantech_php;

create table categorie(
    id serial PRIMARY KEY,
    nom VARCHAR(20),
    label VARCHAR(60)
);

create table produit(
    id serial PRIMARY KEY,
    nom TEXT,
    prix FLOAT,
    description TEXT,
    img TEXT,
    alt TEXT,
    id_categorie int,
    CONSTRAINT fk_produit_categorie FOREIGN KEY (id_categorie) references categorie(id)
);

create table users(
    id serial PRIMARY KEY,
    pseudo VARCHAR(40),
    password VARCHAR(120)
);

insert into users (pseudo,password) values ('mariano','mariano');

insert into categorie (nom) values ('smartphones');
insert into categorie (nom) values ('laptop');
insert into categorie (nom) values ('accessories');
insert into categorie (nom) values ('camera');
insert into categorie (nom) values ('console');
-- vide
insert into categorie (nom) values ('drones');

insert into produit (nom,prix,description,img,alt,id_categorie) values ('Apple iPhone 11 Pro Max 64GB, Midnight Green',1099,'Triple-camera system with 12MP Ultra Wide, Wide, and Telephoto cameras; Night mode, Portrait mode, and 4K video up to 60fps','public/pictures/iphone-11-pro-max-Midnight-Green.jpg','Apple iPhone 11 Pro Max 64GB',1);


insert into produit (nom,prix,description,img,alt,id_categorie) values ('PlayStation 4 Pro 1TB Console',450,'The most advanced PlayStation system ever. PS4 Pro is designed to take your favorite PS4 games and add to them with more power for graphics, performance, or features for your 4K HDR TV, or 1080p HD TV. Ready to level up?','public/pictures/PS4-pro-1tb.jpg','PlayStation 4 Pro 1TB Console',5);
, 

insert into produit (nom,prix,description,img,alt,id_categorie) values ('ASUS VivoBook 15 Thin and Light Laptop, 15.6” FHD Display, Intel i3-1005G1 CPU, 8GB RAM, 128GB SSD, Backlit Keyboard, Fingerprint, Windows 10 Home in S Mode, Slate Gray, F512JA-AS34',399,'Whether at work or play, ASUS VivoBook 15 is the compact laptop that immerses you in whatever you set out to do. Its new frameless four-sided NanoEdge display boasts an ultraslim 5.7mm bezel, giving an amazing 88% screen-to-body ratio for supremely immersive visuals. The ErgoLift hinge design also tilts the keyboard up for more comfortable typing. VivoBook 15 is powered by an Intel core i3 processor to help you get things done with the least amount of fuss. *The actual transfer speed of USB 3.0, 3.1, 3.2 (Gen 1 and 2), and/or Type-C will vary depending on many factors including the processing speed of the host device, file attributes and other factors related to system configuration and your operating environment.','public/pictures/Asus-vivobook-15.jpg','ASUS VivoBook 15 Thin and Light Laptop, 15.6” FHD Display, Intel i3-1005G1 CPU, 8GB RAM, 128GB SSD, Backlit Keyboard, Fingerprint, Windows 10 Home in S Mode, Slate Gray, F512JA-AS34',2);

insert into produit (nom,prix,description,img,alt,id_categorie) values ('Canon EOS M50 Mirrorless Vlogging Camera Kit with EF-M 15-45mm lens, Black',599,'The EOS M50 is a compact interchangeable lens camera for aspiring photographers looking for an easy way to boost the quality of their photos and videos. With clear, high-resolution 4K UHD 24p video, you can capture the incredible details and cinematic moments of your life at home or wherever your adventures take you.','public/pictures/Canon-EOS-M50.jpg','Canon EOS M50 Mirrorless Vlogging Camera Kit with EF-M 15-45mm lens, Black',4);

insert into produit (nom,prix,description,img,alt,id_categorie) values ('Apple Lightning to USB Cable (1 m)',20,'This USB 2.0 cable connects your iPhone, iPad, Mac, AirPods or iPod with Lightning connector to your computer’s USB port for syncing and charging','public/pictures/Apple-Lightning-to-USB Cable.jpg','Apple Lightning to USB Cable (1 m)',3);
;

insert into produit (nom,prix,description,img,alt,id_categorie) values ('Apple Magic Keyboard Silver',56,'Magic Keyboard, Lightning to USB Cable, Bluetooth-enabled Mac computer with OS X 10.11 or later, iOS devices with iOS 9.1 or later','public/pictures/Apple-Magic-Keyboard Silver.jpg','Apple Magic Keyboard Silver',3);

create view real_categories as select * from categorie where id in (select id_categorie from produit);
